files=src/main.c
CC=tcc
CFLAGS=-Wall -Wextra -pedantic -O0

all:
	$(CC) $(CFLAGS) $(files) -o ofetch

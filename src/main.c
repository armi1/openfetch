#include <stdio.h>
#include <stdlib.h>
#include <sys/utsname.h>
#include <time.h>
#include <unistd.h>

#define SECSPERDAY    ((time_t) 86400)
#define SECSPERHOUR   ((time_t) 3600)
#define SECSPERMINUTE ((time_t) 60)

struct ftime_t {
        unsigned long seconds, minutes, hours, days;
};
static struct ftime_t get_uptime();
static char *get_hostname();
static char *get_logname();
static char *get_kernel();

int
main(void) {
        char *logname = get_logname();
        char *hostname = get_hostname();
        char *kernel = get_kernel();
        printf("%s@%s\n==========\n", logname, hostname);
        printf("kernel: %s\n", kernel);
        free(hostname);
        free(logname);
        free(kernel);

        struct ftime_t ftime = get_uptime();
        printf("uptime: %d days, %02d:%02d:%02d\n", ftime.days, ftime.hours,
               ftime.minutes, ftime.seconds);
        return 0;
}

static struct ftime_t
get_uptime() {
        /* a lot of this is basically just copied from the source code for w */
        struct timespec boot_time;
        struct ftime_t format_time; 
        time_t uptime;
        
        if (clock_gettime(CLOCK_BOOTTIME, &boot_time) == -1) {
                perror("[TIME]");
                exit(1);
        }

        uptime = boot_time.tv_sec;
        if (uptime > 59) {

                uptime += 30;

                /* formatting the seconds into days, hours, minutes, and seconds. */
                format_time.days = uptime / SECSPERDAY;
                uptime %= SECSPERDAY;
                format_time.hours = uptime / SECSPERHOUR;
                uptime %= SECSPERHOUR;
                format_time.minutes = uptime / SECSPERMINUTE;
                uptime %= SECSPERMINUTE;
                format_time.seconds = uptime;
        }

        return format_time;
}

/* return value must be freed. */
static char*
get_hostname() {
        char *hostname = (char *)malloc(256);

        if (gethostname(hostname, 255) == -1) {
                perror("[HOSTNAME]");
                exit(1);
        }

        return hostname;
}

/* return value must be freed. */
static char*
get_logname() {
        char *logname = (char *)malloc(256);

        if (getlogin_r(logname, 255) != 0) {
                perror("[LOGNAME]");
                exit(1);
        }

        return logname;
}

/* return value must be freed. */
static char*
get_kernel() {
        struct utsname sysinfo;

        if (uname(&sysinfo) == -1) {
                perror("[KERNEL]");
                exit(1);
        }

        char *output =  (char *)malloc(256);

        snprintf(output, 256, "%s-%s", sysinfo.sysname, sysinfo.release);

        return output;
}
